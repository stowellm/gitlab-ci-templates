---
include:
  - project: cki-project/cki-lib
    file: .gitlab/ci_templates/cki-common.yml
    ref: production

.rules:
  no-merge-train:
    if: $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"
    when: never
  mr-parent:
    if: $CI_MERGE_REQUEST_ID && $CI_PROJECT_ID == $CI_MERGE_REQUEST_PROJECT_ID
  mr-fork:
    if: $CI_MERGE_REQUEST_ID && $CI_PROJECT_ID != $CI_MERGE_REQUEST_PROJECT_ID
  default-branch:
    if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_PATH =~ /^redhat/

# override the default CKI workflow rules to only run in very specific
# circumstances as forks don't have the tagged runners available
workflow:
  rules:
    - !reference [.rules, mr-parent]
    - !reference [.rules, mr-fork]
    - !reference [.rules, default-branch]

default:
  tags:
    - kernel-qe-internal-runner

# explicitly fail MR pipelines running in a fork instead of the parent repo
# most likely a click on https://red.ht/GitLabSSO is needed for RH employees
insufficient-permissions:
  trigger:
    project: redhat/insufficient-permissions
    strategy: depend
  rules:
    - !reference [.rules, mr-fork]

.git-sync:
  extends: .cki_tools
  stage: 🚀
  variables:
    GIT_DEPTH: 100  # works for MRs <= 100 commits on top of the target branch
    FOOTER_CONFIG: |
      name: git-sync
      source_code: https://gitlab.com/redhat/centos-stream/tests/kernel/gitlab-ci-templates/-/blob/main/.gitlab-ci.yml
      faq_name: CKI FAQ
      faq_url: https://cki-project.org/devel-faq/
      slack_name: '#team-kernel-cki'
      slack_url: https://redhat-internal.slack.com/archives/C04KPCFGDTN
      new_issue_url: 'https://gitlab.com/cki-project/cki-tools/-/issues/new?issue[title]=gitlab-ci-templates git-sync issue'
    COMMENT_TEMPLATE_J2: |
      {% set internal_mr_url = 'https://' + env['INTERNAL_GITLAB_HOST'] + '/' +
        env['INTERNAL_GITLAB_PROJECT'] + '/-/tree/' + env['TARGET_BRANCH']
      -%}
      The updated code is synced to the [internal mirror]({{ internal_mr_url }}).

      <details>
      <summary>Click here for an example on how to use the mirrored templates.</summary>

      ```yaml
      ---
      # {{ internal_mr_url }}
      include:
        - project: {{ env['INTERNAL_GITLAB_PROJECT'] }}
          file: .gitlab/base_templates/all_templates.yml
          ref: {{ env['TARGET_BRANCH'] }}
      ```
      </details>
  script:
    - git push --force
      "https://git:${GITLAB_JOB_MIRROR_GITLAB_CI_TEMPLATES_MIRROR_ACCESS_TOKEN}@${INTERNAL_GITLAB_HOST}/${INTERNAL_GITLAB_PROJECT}.git"
      "HEAD:refs/heads/$TARGET_BRANCH"
    - |
      if [[ -n "${CI_MERGE_REQUEST_PROJECT_ID:-}" ]]; then
        # delete old bot notes
        bot_user_id=$(
            gitlab \
                --private-token "${GITLAB_JOB_MIRROR_GITLAB_CI_TEMPLATES_COMMENT_ACCESS_TOKEN}" \
                --output json \
                --fields id \
                current-user get \
            | jq \
                --raw-output \
                '.id'
        )
        gitlab \
            --private-token "${GITLAB_JOB_MIRROR_GITLAB_CI_TEMPLATES_COMMENT_ACCESS_TOKEN}" \
            --output json \
            --fields id,author \
            project-merge-request-note list \
            --project-id "${CI_MERGE_REQUEST_PROJECT_ID}" \
            --mr-iid "${CI_MERGE_REQUEST_IID}" \
            --get-all \
        | jq \
            --raw-output \
            --arg user_id "${bot_user_id}" \
            '.[] | select(.author.id == ($user_id | tonumber)) | .id' \
        | xargs \
            --max-args 1 \
            --no-run-if-empty \
            gitlab \
            --private-token "${GITLAB_JOB_MIRROR_GITLAB_CI_TEMPLATES_COMMENT_ACCESS_TOKEN}" \
            project-merge-request-note delete \
            --project-id "${CI_MERGE_REQUEST_PROJECT_ID}" \
            --mr-iid "${CI_MERGE_REQUEST_IID}" \
            --id
        # post a new note with the URL
        body=$(python3 -m cki_tools.render <<< "${COMMENT_TEMPLATE_J2}")
        footer=$(python3 -m cki_lib.footer --format gitlab)
        python3 -m cki_lib.gitlab \
            --gitlab-url "${CI_SERVER_URL}" \
            --private-token "${GITLAB_JOB_MIRROR_GITLAB_CI_TEMPLATES_COMMENT_ACCESS_TOKEN}" \
            --graphql-query '
              mutation($id: NoteableID!, $body: String!) {
                createNote(input: {noteableId: $id, body:$body, internal: true})
                { errors }
              }' \
            --variables "id=gid://gitlab/MergeRequest/${CI_MERGE_REQUEST_ID}" \
            --variables "body=$(jq -Rs . <<< "${body}${footer}")"
      fi

git-sync-mr:
  extends: .git-sync
  variables:
    TARGET_BRANCH: merge-requests/$CI_MERGE_REQUEST_IID
  rules:
    - !reference [.rules, no-merge-train]
    - !reference [.rules, mr-parent]

git-sync-default-branch:
  extends: .git-sync
  variables:
    TARGET_BRANCH: $CI_COMMIT_BRANCH
  rules:
    - !reference [.rules, default-branch]

# Running those jobs manually until we can use cki and kqe templates
markdownlint:
  extends: .cki_tools
  before_script:
    - markdownlint --version
  script:
    - markdownlint .

yamllint:
  extends: .cki_tools
  before_script:
    - yamllint --version
  script:
    - yamllint .

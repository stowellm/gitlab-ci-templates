---
# File tmt.yml
# This file contains tmt stuff

include:
  - /.gitlab/ci_templates/common.yml

variables:
  KQE_CHECK_POLARION_IDS_CONTAINER_IMAGE: $KQE_TOOLS_CONTAINER_IMAGE
  KQE_CHECK_POLARION_IDS_CONTAINER_VERSION: $KQE_TOOLS_CONTAINER_VERSION
  KQE_TESTING_FARM_CONTAINER_IMAGE: quay.io/testing-farm/cli
  KQE_TESTING_FARM_CONTAINER_VERSION: latest
  KQE_TMT_LINT_CONTAINER_IMAGE: $CKI_TOOLS_CONTAINER_IMAGE
  KQE_TMT_LINT_CONTAINER_VERSION: $CKI_TOOLS_CONTAINER_VERSION
  TESTING_FARM_ARCH: x86_64
  TESTING_FARM_COMPOSE: CentOS-Stream-9
  TESTING_FARM_PLAN_PATH: testing-farm/sanity.fmf

.kqe_testing_farm:
  image: $KQE_TESTING_FARM_CONTAINER_IMAGE:$KQE_TESTING_FARM_CONTAINER_VERSION
  stage: test
  before_script:
    - testing-farm version
  script:
    - |
      if ! test -f "${TESTING_FARM_PLAN_PATH}"; then
        echo "Unable to find the testing plan file: ${TESTING_FARM_PLAN_PATH}"
        exit 1
      fi
      testing-farm request \
        --compose "${TESTING_FARM_COMPOSE}" \
        --arch "${TESTING_FARM_ARCH}" \
        --git-url "${CI_PROJECT_URL}" \
        --git-ref "${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-${CI_COMMIT_SHA}}" \
        --plan "/${TESTING_FARM_PLAN_PATH%.*}"

.kqe_tmt_lint:
  image: $KQE_TMT_LINT_CONTAINER_IMAGE:$KQE_TMT_LINT_CONTAINER_VERSION
  stage: lint
  before_script:
    - !reference [.set_check_only_mr_changes, before_script]
    - tmt --version
    - jq --version
  script:
    - |
      error=0

      if [ "${CHECK_ONLY_MR_CHANGES}" == "true" ]; then
        echo "${MSG_CHECKING_MR_CHANGES}"
        readarray -t directories < <(jq -r '.data.tmt_directories[]' "${CI_JSON_FILE}")
        for directory in "${directories[@]}"; do
          echo "Running tmt lint in ${directory}"
          if ! tmt lint --outcome-only fail "${directory}"; then
            error=1
          fi
        done

      else
        echo "${MSG_CHECKING_ALL_REPOSITORY}"
        if ! tmt lint --outcome-only fail "${directory}"; then
          error=1
        fi
      fi

      if [ "${error}" -ne "0" ]; then
        echo "Please, fix the syntax problems as separate commit if they were not caused by your changes."
        exit 1
      fi

.kqe_check_polarion_ids:
  image: $KQE_CHECK_POLARION_IDS_CONTAINER_IMAGE:$KQE_CHECK_POLARION_IDS_CONTAINER_VERSION
  stage: lint
  before_script:
    - tmt --version
    - python3 --version
  script:
    - tmt tests export --how json > tmt_tests.json
    - check_polarion_ids --input tmt_tests.json

.kqe_check_tmt_plan_documentation:
  image: $KQE_TMT_LINT_CONTAINER_IMAGE:$KQE_TMT_LINT_CONTAINER_VERSION
  stage: test
  before_script:
    - tmt --version
    - python3 --version
  # yamllint disable rule:line-length
  script:
    - |
      echo "INFO: going to check if test plans documentation needs update."
      echo "in case of problems, open an issue at: https://gitlab.com/cki-project/cki-tools/-/issues"
      python3 -m cki_tools.tmt_generate_test_plans_doc --plans-dir .
      if ! git diff --exit-code; then
        echo "Please, run 'podman run --rm -it -v .:/test-plans:Z  quay.io/cki/cki-tools:production python3 -m cki_tools.tmt_generate_test_plans_doc --plans-dir /test-plans' to update the documentation."
        exit 1
      fi
